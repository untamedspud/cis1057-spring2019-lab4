#include <stdio.h>
#include <stdlib.h>
#include "../include/cis1057.h"

#define ADJUSTMENT 0.5555555

double get_fahrenheit_value_from_user( void );
double convert_fahrenheit_to_celsius( double fahrenheit );
void print_result( double fah, double cel );

int main()
{
	double fahrenheit, celsius;

	fahrenheit = get_fahrenheit_value_from_user();
	celsius = convert_fahrenheit_to_celsius( fahrenheit );
	print_result( fahrenheit, celsius );

	return EXIT_SUCCESS;
}

double get_fahrenheit_value_from_user( void )
{
	double f;

	printf( "Enter a temperature in fahrenheit: " );
	scanf( "%lf", &f );

	return f;
}

double convert_fahrenheit_to_celsius( double fahrenheit )
{
	return ( fahrenheit - 32.0 ) * ADJUSTMENT;
}

void print_result( double fah, double cel )
{
	printf( "%0.2lf degrees fahrenheit is %0.2lf in celsius.\n", fah, cel );
	printf( "%lfF = %lf.\n", fah, cel );

	return;
}
